'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')


/*
var actionCodeSettings = {
    url: process.env.redirectUrl,
    iOS: {
        bundleId: 'com.example.ios'
    },
    android: {
        packageName: 'com.example.android',
        installApp: true,
        minimumVersion: '12'
    },
    handleCodeInApp: true
};
*/
module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    firebase_apiKey: '"AIzaSyCDKwyNZi0QF51Cj2F2eHwgRDBTllYEU-w"',
    firebase_authDomain: '"nextgenplanning-cc7a5.firebaseapp.com"',
    firebase_databaseURL: '"https://nextgenplanning-cc7a5.firebaseio.com"',
    firebase_projectId: '"nextgenplanning-cc7a5"',
    firebase_storageBucket: '"nextgenplanning-cc7a5.appspot.com"',
    firebase_messagingSenderId: '"329096183513"',
    redirectUrl: '"http://localhost:8080/#/home"',
    firebase_actionCodeSettings:  {
        url: '"http://localhost:8080"',
    }

})
