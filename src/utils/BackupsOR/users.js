import decode from 'jwt-decode';
import axios from 'axios'
import { getAccessToken } from './auth';

// const BASE_URL = 'http://rest.nextgen.pyrotechsolutions.com/user';
const BASE_URL = 'http://rest.nextgen.com/user';


export {getUsers};

function getUsers(){
    var token = getAccessToken();
    var jwt = decode(token);
    console.log(jwt.sub);
    var form = new FormData();
    form.append('user_id',jwt.sub);
    return axios({
        method: 'post',
        url: BASE_URL + '/get-user',
        headers: {
            Authorization: 'Bearer '+ token,
        },
        data:form
    });
}
