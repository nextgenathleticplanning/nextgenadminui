if (!Array.isArray) {
    Array.isArray = function (arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    };
}
function isObject(arg) {
    return Object.prototype.toString.call(arg) === '[object Object]';
}
String.prototype.replaceAll = function (strReplace, strWith) {
    // See http://stackoverflow.com/a/3561711/556609
    var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    var reg = new RegExp(esc, 'ig');
    return this.replace(reg, strWith);
};
function strrpos(haystack, needle, offset) {
    // github: https://github.com/kvz/locutus/blob/master/src/php/strings/strrpos.js
    //  discuss at: http://locutus.io/php/strrpos/
    // original by: Kevin van Zonneveld (http://kvz.io)
    // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //    input by: saulius
    //   example 1: strrpos('Kevin van Zonneveld', 'e')
    //   returns 1: 16
    //   example 2: strrpos('somepage.com', '.', false)
    //   returns 2: 8
    //   example 3: strrpos('baa', 'a', 3)
    //   returns 3: false
    //   example 4: strrpos('baa', 'a', 2)
    //   returns 4: 2

    var i = -1
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle) // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle)
    }
    return i >= 0 ? i : false
}
function getValue(array, key, dVal) {
    if (Array.isArray(key)) {
        var lastKey = key.pop();
        var keyLength = key.length;
        for (var i = 0; i < keyLength; i++) {
            var keyPart = key[i];
            var array = getValue(array, keyPart);
        };
        var key = lastKey;
    }
    var defaultValue = (dVal == undefined) ? null : dVal;
    var pos = strrpos(key, '.');
    if (pos !== false) {
        var newKey = key.substr(0, pos);
        array = getValue(array, newKey, defaultValue);
        var key = key.substr(pos + 1);
    }
    return ((Array.isArray(array) || isObject(array)) && (array[key] !== undefined && array[key] !== null)) ? array[key] : defaultValue;
}
export function isEmpty(obj, extra) {
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    if (obj == null || typeof obj == 'undefined') {
        return true;
    }
    if (Array.isArray(obj) && !obj.length) {
        return true;
    }
    if (isObject(obj) && !Object.keys(obj).length) {
        return true;
    }
    if (!obj.length) {
        return true;
    }
    if (extra !== undefined && obj == extra) {
        return true;
    }
    return false;
}