import decode from 'jwt-decode';
import axios from 'axios'
import { getAccessToken } from './auth';


export {getUsers};

function getUsers(){
    var token = getAccessToken();
    var jwt = decode(token);
    console.log(jwt.sub);
    var form = new FormData();
    form.append('user_id',jwt.sub);
    return axios({
        method: 'post',
        url: process.env.REST_URL + 'user/get-user',
        headers: {
            Authorization: 'Bearer '+ token,
        },
        data:form
    });
}
