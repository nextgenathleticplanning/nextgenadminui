// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import Vuelidate from 'vuelidate'
import store from './store'
import VueSession from 'vue-session'
import 'native-toast/dist/native-toast.css'
Vue.use(VueSession, {persist: true})
Vue.use(Vuelidate)
import firebase from "firebase";
// console.log(process.env.firebase_apiKey);
firebase.initializeApp({
  apiKey: process.env.firebase_apiKey,
  authDomain: process.env.firebase_authDomain,
  databaseURL: process.env.firebase_databaseURL,
  projectId: process.env.firebase_projectId,
  storageBucket: process.env.firebase_storageBucket,
  messagingSenderId: process.env.firebase_messagingSenderId
});
let app;

require("./assets/plugins/ionicons/css/ionicons.min.css")
require("./assets/dist/css/styles.css")

// require("./assets/plugins/pace/pace.min.js")

// require("./assets/plugins/jquery-ui/jquery-ui.min.js")
// require("./assets/plugins/slimscroll/jquery.slimscroll.min.js")
// require("./assets/plugins/js-cookie/js.cookie.js")
// require("./assets/js/theme/apple.min.js")
// require("./assets/js/apps.min.js")

Vue.config.productionTip = false
Vue.http = Vue.prototype.$http = axios

/* eslint-disable no-new */
firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      template: '<App/>',
      components: { App },
      router,
      store
    })
  }
});