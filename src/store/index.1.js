import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase';
Vue.use(Vuex)

const database = firebase.database();
const profileRef = database.ref('profile');

let store =  new Vuex.Store({
    state: {
        flash:[],
        user: null,
        profile:null,
    },
    getters: {
        isLoggedIn:function(state){
            return (state.user != null);
        },
        getUser:function(state){
            return state.user;
            // return store._vm.$session.get('user');
        },
        getflash:function(state){
            return state.flash;
        },
        getUserProfile:function(state){
            return state.profile;
            var user = state.user;
            if(user == null){
                state.profile = {
                    name: '',
                    title: '',
                    photoUrl: require('../assets/img/user/default-user.png'),
                }
            }else{
                state.profile = {
                    name: user.displayName,
                    title: '',
                    photoUrl: user.photoURL == null ? require('../assets/img/user/default-user.png') : user.photoURL,
                }
            }
            return state.profile;
        }
    },
    mutations: {
        setUser(state,payload){
            console.log('user payload', payload);
            // this._vm.$session.start();
            // store._vm.$session.set('user',payload);
            state.user = (payload == undefined) ? firebase.auth().currentUser : payload;
            // if(state.user){
            //     let userId = state.user.uid;
            //     state.profile = database.ref('/profile/' + userId).once('value').then(function (snapshot) {
            //         // var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
            //         return snapshot.val();
            //     });  
            // }
            // state.isLoggedIn = (payload != null)
        }, 
        setProfile(state){

        },
        saveFlash(state,payload){
            state.flash.push(payload); 
        },
        test(state){
            state.profile = {
                displayName : 'Rocky Road',
                title: 'Owner',
                callsign:'test',
                photoURL: 'https://media.istockphoto.com/photos/asian-women-profile-picture-id139662784',
            }
            console.log('test profile-change',state.profile);
        }
    },
    actions: {
        setUser(context){
            context.commit('setUser');
        }
    }
});

export default store