import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'
import { trim } from 'locutus/php/strings'
const merge = require('webpack-merge')
Vue.use(Vuex)

let store = new Vuex.Store({
    state: {
        flash: [],
        user: null,
        profile: {
            title:'',
            fullName: '',
            firstName: '',
            concName:'',
            middleName: '',
            lastName: '',
            phoneNumber:'',
            address:'',
            placeId:''
        },
        loaded:false,
    },
    getters: {
        isLoggedIn: function (state) {
            return (state.user != null);
        },
        getUser: function (state) {
            return state.user;
            // return store._vm.$session.get('user');
        },
        getflash: function (state) {
            return state.flash;
        },
        getUserProfile: function (state) {
            var profile = state.profile;
            state.profile.fullName = trim(profile.firstName + ' ' + profile.middleName + ' ' + profile.lastName);
            state.profile.concName = trim(profile.firstName + ' ' + profile.lastName);
            return state.profile;
        }
    },
    mutations: {
        setUser(state, payload) {
            console.log('user payload', payload);
            // this._vm.$session.start();
            // store._vm.$session.set('user',payload);
            state.user = (payload == undefined) ? firebase.auth().currentUser : payload;
            if(state.user == undefined){
                state.user = {
                    name: '',
                    title: '',
                    photoUrl: require('../assets/img/user/default-user.png'),
                }
            }else{
                state.user.photoUrl = state.user.photoURL == null ? require('../assets/img/user/default-user.png') : state.user.photoURL;
            }
        },
        setProfile(state, user) {
            if(user != undefined && user != null){
                firebase.database().ref('/profile/' + user.uid).on('value', function (snapshot) {
                    var data = snapshot.val();
                    if (data != undefined && data != null){
                        state.profile = merge(state.profile,data);
                    }
                });
            }
        },
        saveFlash(state, payload) {
            state.flash.push(payload);
        },
        test(state) {
            state.profile = {
                displayName: 'Rocky Road',
                title: 'Owner',
                callsign: 'test',
                photoURL: 'https://media.istockphoto.com/photos/asian-women-profile-picture-id139662784',
            }
            console.log('test profile-change', state.profile);
        }
    },
    actions: {
        setUser(context) {
            context.commit('setUser');
        }
    }
});

export default store