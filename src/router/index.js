import Vue from 'vue';
import firebase from 'firebase'
import Router from 'vue-router';
import store from '../store'


import Login from '@/components/user/Login';
import Register from '@/components/user/Register';
import ForgotPassword from '@/components/user/ForgotPassword';
import Home from '@/components/Home';
import EmailVerify from '@/components/user/EmailVerify';
import Profile from '@/components/user/Profile';
import UserCreate from '@/components/admin/user/Create';
import Dashboard from '@/components/admin/user/Dashboard';

Vue.use(Router);

let router = new Router({
    // mode: 'history',
    routes: [
        {
            path: '*',
            redirect: '/login'
        },
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            meta: {
                nonUserOnly: true
            }
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
            meta: {
                nonUserOnly: true
            }
        },
        {
            path: '/forgot-password',
            name: 'ForgotPassword',
            component: ForgotPassword,
            meta: {
                nonUserOnly: true
            }
        },
        {
            path: '/verify',
            name:'Verify',
            component: EmailVerify,
            // meta: {
            //     requiresAuth: true
            // }
        },
        {
            path: '/home',
            name: 'Home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/profile',
            name: 'Profile',
            component: Profile,
            meta: {
                requiresAuth: true
            }
        }, ,
        {
            path: '/user-create',
            name: 'UserCreate',
            component: UserCreate,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/user-dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        }
    ],
});

router.beforeEach(function(to, from, next){
    let user = firebase.auth().currentUser;
    let requiresAuth = to.matched.some(function(record){
        return record.meta.requiresAuth;
    });
    let nonUserOnly = to.matched.some(function (record) {
        return record.meta.nonUserOnly;
    });
    // console.log(this);
    store.commit('setUser', user);
    store.commit('setProfile', user);

    // let test =  store.getters.user;
    // console.log(test);
    // Vue.$session.set('user', user);
    // localStorage.setItem('user', JSON.stringify(user));

    // console.log('user ==>',user);
    // console.log('requiresAuth ==>', requiresAuth);
    // console.log('nonUserOnly ==>', nonUserOnly);
    // console.log('from ==>', from);
    /*
    * Logic
    * If it's a nonUserOnly Page (Register, Reset Password, Login, etc. ) make sure they're not logged in else Send them to the home page
    * If the page is a logged in user only page then verify their logged in and if not send them to the login page
    * If the page is a logged in user only page and the user is logged in verify they've verified their email address
    * If the page doesn't require auth OR restricted then let them pass. 
    * If we miss something, let them pass and we'll fix holes as they occur
    */
    if (nonUserOnly && (user != null)){
        console.log('nonUserOnly && user');
        next({ path: '/home' });
    } else if (requiresAuth && (user == null)) {
        console.log('requiresAuth && !user');
        next({ path: '/login' });
    } else if (requiresAuth && (user != null)) {
        console.log('requiresAuth && user');
        if (!user.emailVerified){
            next({ path: '/verify' });
        }else{
            next();
        }
    } else if (!requiresAuth) {
        console.log('!requiresAuth');
        next();
    }else{
        next();
    }
})

export default router